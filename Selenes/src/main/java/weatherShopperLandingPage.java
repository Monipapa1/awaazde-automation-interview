import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import junit.framework.Assert;
import junit.framework.TestCase;
/**
 * 
 */

/**
 * @author Monisha
 *
 */
public class weatherShopperLandingPage {
	static WebDriver driver;
	/**
	 * Test the landng page of the website
	 */
	
	@BeforeClass
	public static void Beforeclass() {
		System.setProperty("webdriver.chrome.driver", "C:\\selenium\\chromedriver.exe");
		ChromeOptions options = new ChromeOptions();
		options.addArguments("--disable--notifications");
		driver = new ChromeDriver();
	}
	
	@AfterClass
	public static void Afterclass() {
		driver.quit();
	}
	
	@Test
	public void testContent() {
		// TODO Auto-generated method stub
		driver.manage().window().maximize();
		driver.get("http://weathershopper.pythonanywhere.com/");
		
		//Test page heading
		WebElement heading = driver.findElement(By.cssSelector("h2"));
		String actualHeadingText = heading.getText();
		String expectedHeadingText = "Current temperature";
		Assert.assertEquals("The Heading of the page is not as expected",expectedHeadingText, actualHeadingText );	
		
		//Test Page Title
		String actualTitle = driver.getTitle();
		String expectedTilte = "Current Temperature";
		Assert.assertEquals("The Heading of the page is not as expected",expectedTilte, actualTitle );	
		
	}

}
