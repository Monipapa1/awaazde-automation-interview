import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.WebDriverWait;

import junit.framework.Assert;

/**
 * 
 */

/**
 * @author hp
 *
 */
public class weatherShopperOpeningURL {

	static WebDriver driver;
	
	@BeforeClass
	public static void Beforeclass() {
		System.setProperty("webdriver.chrome.driver", "C:\\selenium\\chromedriver.exe");
		ChromeOptions options = new ChromeOptions();
		options.addArguments("--disable--notifications");
		driver = new ChromeDriver();
	}
	
	@AfterClass
	public static void Afterclass() {
		driver.quit();
	}
	@Test
	public void Weathershopperproducts() {
		
		
		driver.manage().window().maximize();
		driver.get("http://weathershopper.pythonanywhere.com/");
		
		
		
		WebElement Moisturizers = driver.findElement(By.xpath("//button[contains(.,'Buy moisturizers')]"));
		Moisturizers.click();
		
		//Test page url
		String actualUrl = driver.getCurrentUrl();
		String expectedHeadingText = "http://weathershopper.pythonanywhere.com/moisturizer";
		Assert.assertEquals("The URL of the page is not as expected",expectedHeadingText, actualUrl );	
		


	}
	
	@Test
	public void Weathershopperproducts1() {
		
		driver.manage().window().maximize();
		driver.get("http://weathershopper.pythonanywhere.com/");
		
		
		
		WebElement Moisturizers = driver.findElement(By.xpath("//button[contains(.,'Buy moisturizers')]"));
		Moisturizers.click();
	
		Moisturizers = driver.findElement(By.xpath("//button[@onclick=\"addToCart('Vassily Aloe Attack',199)\"]"));
		Moisturizers.click();
		
		WebElement cart = driver.findElement(By.xpath("//span[@id='cart']"));
		cart.click();
		
		WebDriverWait wait2 = new WebDriverWait(driver, 10);

		cart = driver.findElement(By.xpath("//span[contains(.,'Pay with Card')]"));
		cart.click();
		
		String actualUrl = driver.getCurrentUrl();
		String expectedHeadingText = "http://weathershopper.pythonanywhere.com/cart";
		Assert.assertEquals("The URL of the page is not as expected",expectedHeadingText, actualUrl );	
		
		
         driver.navigate().back();

         String actualUrl2 = driver.getCurrentUrl();
 		String expectedHeadingText2 = "http://weathershopper.pythonanywhere.com/moisturizer";
 		Assert.assertEquals("The URL of the page is not as expected",expectedHeadingText2, actualUrl2 );	
 		
		
		
	}
	
	
	

}
